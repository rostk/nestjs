import { ConflictException } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import { User } from "./auth.entity";
import { CredentialDto } from "./dto/auth-credential";


@EntityRepository(User)
export class AuthRepository extends Repository<User> {


    async singUp(credentialDto: CredentialDto): Promise<void> {
        const { username, password } = credentialDto;
        const newUser = new User();
        newUser.username = username;
        newUser.password = password;
        try {
            await newUser.save()
        } catch (e) {
            throw new ConflictException(e)
        }


    }
}