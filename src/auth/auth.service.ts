import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthRepository } from './auth.repository';
import { CredentialDto } from './dto/auth-credential';

@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(AuthRepository)
        private authRepository: AuthRepository
    ){}


    singUp(credentialDto: CredentialDto): Promise<void>{
        return this.authRepository.singUp(credentialDto)
    }
}
