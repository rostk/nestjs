import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CredentialDto } from './dto/auth-credential';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService){}


    @Post('singup')
    singUp(@Body() credentialDto: CredentialDto): Promise<void>{
        return this.authService.singUp(credentialDto);
    }
}
