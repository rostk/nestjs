import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskRepository } from './task.repository';
import { Task } from './task.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TasksService {

    constructor(
        @InjectRepository(TaskRepository)
        private taskRepository: TaskRepository,
    ) { }
    getAllTasks(): Promise<Task[]> {
        return this.taskRepository.find();
    }

    createTask(createTaskDto: CreateTaskDto): Promise<Task> {
        return this.taskRepository.createTask(createTaskDto);
    }

    async getTaskById(id: number): Promise<Task> {
        const task = await this.taskRepository.findOne(id)
        if (!task) {
            throw new NotFoundException(`Task with id ${id} not found`)
        }
        return task;
    }

    async deleteTaskById(id: number): Promise<void> {
        const task = await this.taskRepository.delete(id);
        if (task.affected === 0) {
            throw new NotFoundException(`Task with id ${id} not found`)
        }
    }

    async updateTask(id: number, createTaskDto: CreateTaskDto): Promise<Task> {
        const { title, description } = createTaskDto;
        const task = await this.getTaskById(id);
        task.title = title;
        task.description = description;
        await task.save();
        return task;
    }



}
